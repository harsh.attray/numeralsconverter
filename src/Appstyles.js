import styled from "styled-components";

export const MainContainer = styled.main`
  width: 90%;
  max-width: 1200px;
  margin: 0 auto;
  @media only screen and (max-width: 1200px) {
    margin: 0;
    max-width: 100%;
    width: 100%;
  }
`;

export const HeaderContainer = styled.header`
  display: grid;
  grid-template-columns: 10% auto 30%;
  align-items: center;
  background-color: black;
  color: white;
`;

export const LogoContainer = styled.div`
  width: 80px;
`;

export const NumeralsContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;
