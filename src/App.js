import React from "react";
import logo from "./logo.svg";
import RomantoIntegerComponent from "./components/RomantoIntegerComponent/RomantoIntegerComponent";
import IntegertoRomanComponent from "./components/IntegerToRomanComponent/IntegerToRomanComponent";
import {
  MainContainer,
  LogoContainer,
  HeaderContainer,
  NumeralsContainer
} from "./Appstyles";

function App() {
  return (
    <MainContainer>
      <HeaderContainer>
        <LogoContainer>
          <img src={logo} className="App-logo" alt="logo" />
          <p>Numerals Container</p>
        </LogoContainer>
      </HeaderContainer>
      <NumeralsContainer>
        <RomantoIntegerComponent />
        <IntegertoRomanComponent />
      </NumeralsContainer>
    </MainContainer>
  );
}

export default App;
