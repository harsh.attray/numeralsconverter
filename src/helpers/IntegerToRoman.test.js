import { IntToRoman } from "./integerToRoman";

describe("Calculate Value functionality", () => {
  it("should should return MMMfor 3000", () => {
    expect(IntToRoman(3000)).toEqual("MMM");
  });

  it("should should return MXL for 1040", () => {
    expect(IntToRoman(1040)).toEqual("MXL");
  });

  it("should should return DXL for 540", () => {
    expect(IntToRoman(540)).toEqual("DXL");
  });
});
