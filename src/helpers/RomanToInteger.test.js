import RomanToInteger from "./romanToInteger";

describe("Calculate Value functionality", () => {
  it("should should return 4000 for MMMM", () => {
    expect(RomanToInteger("MMMM")).toEqual(4000);
  });

  it("should should return 1040 for MXL", () => {
    expect(RomanToInteger("MXL")).toEqual(1040);
  });

  it("should should return 540 for DXL", () => {
    expect(RomanToInteger("DXL")).toEqual(540);
  });
});
