const RomanToInteger = input => {
  var result = 0;
  const romanValues = [
    ["M", 1000],
    ["CM", 900],
    ["D", 500],
    ["CD", 400],
    ["C", 100],
    ["XC", 90],
    ["L", 50],
    ["XL", 40],
    ["X", 10],
    ["IX", 9],
    ["V", 5],
    ["IV", 4],
    ["I", 1]
  ];
  romanValues.map(val => {
    while (input.indexOf(val[0]) === 0) {
      result += val[1];
      input = input.replace(val[0], "");
    }
  });
  return result;
};

export default RomanToInteger;
