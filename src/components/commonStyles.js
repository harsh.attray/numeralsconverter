import styled from "styled-components";

export const HeaderContainer = styled.header`
  display: grid;
  grid-template-columns: 10% auto 30%;
  align-items: center;
  background-color: black;
  color: white;
`;

export const LabelContainer = styled.label`
  border-radius: 4px;

  & p {
    margin: 20px;
  }
`;

export const NumeralsContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const ButtonStyles = styled.span`
  margin: 20px;
  & input {
    border-radius: 4px;
    background-color: black;
    color: white;
  }
`;
