import React from "react";
import { IntToRoman } from "../../helpers/integerToRoman";
import { LabelContainer, ButtonStyles } from "../commonStyles";

export default class IntegertoRomanComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { rValue: "", iValue: 0, hasError: false };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ rValue: event.target.value });
  }
  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    console.log("Error", error, info);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ iValue: IntToRoman(this.state.rValue) });
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <LabelContainer>
            <p>Enter Integer Numerals:</p>
            <br />
            <input
              type="text"
              value={this.state.value}
              onChange={this.handleChange}
            />
          </LabelContainer>
          <ButtonStyles>
            <input type="submit" value="Submit" />
          </ButtonStyles>
        </form>
        <p> This equates to:</p>
        <div>{this.state.iValue}</div>
      </div>
    );
  }
}
